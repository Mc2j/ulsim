<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class FirstMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lat');
            $table->string('lng');
            $table->string('name');
            $table->text('description');
        });

        \Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('point_id');
            $table->text('description');
            $table->string('image');
            $table->string('owner')->nullable();
        });

        \Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('text');
            $table->integer('point_id');
            $table->timestamp('created_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('points');
        Schema::drop('photos');
        Schema::drop('articles');
    }
}
