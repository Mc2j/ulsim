<?php

namespace App\Http\Controllers;

use App\Models\Point;
use Illuminate\Routing\Controller as BaseController;

class PointController extends BaseController
{
    public function index() {
        $points = Point::get();

        return response()->json($points);
    }

    public function detail($id) {
        $point = Point::find($id);

        return view('points.detail', [
            'point' => $point,
        ]);
    }
}
