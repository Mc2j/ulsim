<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Point
 * @package App\Models
 *
 * @property integer photo_count
 * @property integer article_count
 *
 * @mixin \Eloquent
 */

class Point extends Model
{
    public $table = 'points';
    public $timestamps = false;
    protected $fillable = ['lat', 'lng', 'name', 'description'];
    protected $visible = ['id', 'lat', 'lng', 'name', 'description', 'photos', 'photo_count', 'article_count'];

    public function photos() {
        return $this->hasMany(Photo::class, 'point_id');
    }

    public function articles() {
        return $this->hasMany(Article::class, 'point_id');
    }

    public function toArray()
    {
        $this->photo_count = $this->photos()->getResults()->count();
        $this->article_count = $this->articles()->getResults()->count();

        return parent::toArray();
    }
}