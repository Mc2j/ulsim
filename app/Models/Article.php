<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $table = 'articles';
    public $timestamps = false;
    protected $fillable = ['title', 'text', 'point_id', 'created_at'];
    protected $visible = ['id', 'title', 'text', 'point_id', 'created_at'];
}