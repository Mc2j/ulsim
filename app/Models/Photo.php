<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 * @package App\Models
 *
 * @mixin \Eloquent
 */

class Photo extends Model
{
    public $table = 'photos';
    public $timestamps = false;
    protected $fillable = ['point_id', 'description', 'image', 'owner'];
}