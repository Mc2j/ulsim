<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

class Photos extends Section implements Initializable
{
    /**
     * @var \App\Models\Photo
     */
    protected $model = '\App\Models\Photo';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 500, function() {
            return \App\Models\Photo::count();
        });

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }

    protected $checkAccess = false;
    protected $title = 'Фотографии';
    protected $alias = 'photos';

    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::link('id', '#')->setWidth('30px'),
                AdminColumn::text('point_id', 'Точка'),
                AdminColumn::text('description', 'Описание'),
                AdminColumn::text('image', 'Фотография'),
                AdminColumn::text('owner', 'Авторские права'),
                AdminColumn::text('year', 'Год')
            )->paginate(20);
    }

    /**
     * @param int $id
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('id', 'ID')->setReadonly(1),
            AdminFormElement::selectajax('point_id', 'Точка')->setModelForOptions('\App\Models\Point')->setDisplay('name')->required(),
            AdminFormElement::image('image', 'Фотография')->required(),
            AdminFormElement::textarea('description', 'Описание'),
            AdminFormElement::text('owner', 'Авторские права'),
            AdminFormElement::number('year', 'Год'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    //заголовок для создания записи
    public function getCreateTitle()
    {
        return 'Добавить фотографию';
    }

    // иконка для пункта меню - шестеренка
    public function getIcon()
    {
        return 'fa fa-gear';
    }
}