$(document).ready(function () {

    function App() {
        this.init = function(){
            this.resizeBlocks();
            this.markers = {};

            this.markerIcons = {
                'photos': {
                    url: '/images/icons.png',
                    size: new google.maps.Size(64, 64),
                    origin: new google.maps.Point(394, 284),
                    anchor: new google.maps.Point(17, 34),
                },
                'articles': {
                    url: '/images/icons.png',
                    size: new google.maps.Size(64, 64),
                    origin: new google.maps.Point(141, 32),
                    anchor: new google.maps.Point(17, 34),
                },
                'mixed': {
                    url: '/images/icons.png',
                    size: new google.maps.Size(64, 64),
                    origin: new google.maps.Point(394, 705),
                    anchor: new google.maps.Point(17, 34),
                }
            };

            this.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 54.314404434020695, lng: 48.39731258773804},
                zoom: 16,
                zoomControl: false,
                scaleControl: false,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                mapTypeControl: false,
                streetViewControl: false,
                styles: [{
                    featureType: "landscape",
                    stylers: [
                        { visibility: "off" }
                    ]},{
                    featureType: "poi",
                    stylers: [
                        { visibility: "off" }
                    ]},{
                    featureType: "transit",
                    stylers: [
                        { visibility: "off" }
                    ]}
                ]
            });

            $.ajax({
                'url': '/points',
                success: (function (points) {
                    console.log(this);
                    $.each(points, (function(i, point){

                        var icon = '';
                        if (point.photo_count > 0 && point.article_count > 0) {
                            icon = 'mixed';
                        } else if (point.photo_count > 0) {
                            icon = 'photos';
                        } else {
                            icon = 'articles';
                        }

                        this.markers[point.id] = new google.maps.Marker({
                            position: new google.maps.LatLng(point.lat, point.lng),
                            map: this.map,
                            title: point.name,
                            icon: this.markerIcons[icon]
                        }).addListener('click', (function() {
                            this.openInfo('point', point.id);
                        }).bind(this));
                    }).bind(this));
                }).bind(this)
            });

            this.bindEvents();
        };

        this.openInfo = function (entity, entity_id) {
            if (entity == 'point') {
                $.ajax({
                    url: '/points/'+entity_id,
                    success: (function(response){
                        $('[data-side-content]').html(response);
                        $('[data-side]').show();
                        this.bindLitebox();
                    }).bind(this)
                });
            }
        };

        this.bindEvents = function () {

            $('[data-side-close]').on('click', function (event) {
                event.preventDefault();
                $('[data-side]').hide();
            });

            $(window).resize((function () {
                this.resizeBlocks();
            }).bind(this));
        };

        this.bindLitebox = function () {
            $('.litebox').fancybox();
        };

        this.resizeBlocks = function () {
            $('[data-container]').css({
                height: $('body').height() - $('[data-header]').height()
            });
        };
    }

    app = new App();
    app.init();
});