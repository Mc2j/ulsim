<!DOCTYPE html>
<html>
    <head>
        <title>Ulsim</title>
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/jquery.fancybox.css">
    </head>
    <body>
        @yield('content')
        <script async defer src="//maps.googleapis.com/maps/api/js"></script>
        <script src="//code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="/js/jquery.fancybox.min.js"></script>
        <script src="/js/script.js"></script>
    </body>
</html>