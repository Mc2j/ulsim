<div>
    <h2>{{$point['name']}}</h2>
    @if(strlen($point['description']) > 0)
        <div>{{$point['description']}}</div>
    @endif
    @if(count($point['photos']) > 0)
        <div>
            @foreach($point['photos'] as $photo)
                <a class = "litebox" rel = "gallery-{{$point['id']}}" href = "/{{$photo['image']}}">
                    <img class = "photo-preview" src = "/{{$photo['image']}}">
                </a>
            @endforeach
        </div>
    @endif
</div>