@extends('layouts.index')

@section('content')
    <div class = 'header' data-header = ""></div>
    <div class = 'container' data-container = "">
        <div id = 'map'></div>
        <div class = 'side' data-side="">
            <div data-side-close = "" class = 'side-close'>Close</div>
            <div data-side-content = "" class = 'side-content'>Close</div>
        </div>
    </div>
@endsection